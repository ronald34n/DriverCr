package com.micrium.properties;

import java.io.FileInputStream;
import java.util.Properties;
import micrium.aes.AlgoritmoAES;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class ConexionLocalProperties {

    private static final Logger LOGGER = Logger.getLogger(ConexionLocalProperties.class);

    private static final Properties prop = new Properties();

    private static AlgoritmoAES aes = new AlgoritmoAES();

    static {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("etc/conexionLocal.properties");
			System.out.println("Cargando parametros de propiedades ...");
			LOGGER.info("archivo de conexion cargado correctamente");
            prop.load(fis);
			
        } catch (Exception e) {
            LOGGER.fatal("[Fallo al leer el archivo conexionLocal.properties]", e);
            System.exit(-3);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (Exception e) {
				LOGGER.error("Error al cerrar el archivo de propiedades",e);
            }
        }
    }

    public static final String LOCAL_URL = prop.getProperty("local.Url");
    public static final String LOCAL_DRIVERCLASSNAME = prop.getProperty("local.DriverClassName");
    public static final String LOCAL_JMXENABLED = prop.getProperty("local.JmxEnabled");
    public static final String LOCAL_LOGABANDONED = prop.getProperty("local.LogAbandoned");
    public static final String LOCAL_MAXACTIVE = prop.getProperty("local.MaxActive");
    public static final String LOCAL_MAXWAIT = prop.getProperty("local.MaxWait");
    public static final String LOCAL_MAXIDLE = prop.getProperty("local.MaxIdle");
    public static final String LOCAL_MINIDLE = prop.getProperty("local.MinIdle");
    public static final String LOCAL_MINEVICTABLEIDLETIMEMILLIS = prop.getProperty("local.MinEvictableIdleTimeMillis");
    public static final String LOCAL_REMOVEABANDONED = prop.getProperty("local.RemoveAbandoned");
    public static final String LOCAL_REMOVEABANDONEDTIMEOUT = prop.getProperty("local.RemoveAbandonedTimeout");
    public static final String LOCAL_TESTONBORROW = prop.getProperty("local.TestOnBorrow");
    public static final String LOCAL_TESTONRETURN = prop.getProperty("local.TestOnReturn");
    public static final String LOCAL_TESTWHILEIDLE = prop.getProperty("local.TestWhileIdle");
    public static final String LOCAL_TIMEBETWEENEVICTIONRUNSMILLIS = prop.getProperty("local.TimeBetweenEvictionRunsMillis");
    public static final String LOCAL_VALIDATIONINTERVAL = prop.getProperty("local.ValidationInterval");
    public static final String LOCAL_VALIDATIONQUERY = prop.getProperty("local.ValidationQuery");
    public static final String LOCAL_JDBCINTERCEPTORS = prop.getProperty("local.JdbcInterceptors");
    public static final String LOCAL_USER = aes.desencriptar(prop.getProperty("local.user"));
    public static final String LOCAL_PASSWORD = aes.desencriptar(prop.getProperty("local.password"));
}
