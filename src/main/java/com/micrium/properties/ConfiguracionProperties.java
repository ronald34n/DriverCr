package com.micrium.properties;

import java.io.FileInputStream;
import java.io.Serializable;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class ConfiguracionProperties implements Serializable {

	private static final Logger LOGGER = Logger.getLogger(ConfiguracionProperties.class);

	private static final Properties prop = new Properties();
	

	static {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("etc/configuracion.properties");
			prop.load(fis);
		} catch (Exception e) {
			LOGGER.fatal("[Fallo al leer el archivo configuracion.properties]", e);
			System.exit(-3);
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (Exception e) {
				LOGGER.error("Error al cerrar el archivos de propiedades",e);
			}
		}
	}
	public static final int CANTIDAD_PARAMETROS = 10;

	public static final String EXPRESION_VALIDAR_TRAMA = prop.getProperty("EXPRESION_VALIDAR_TRAMA");

}
