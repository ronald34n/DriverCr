package com.micrium.notificacion;

import com.micrium.bd.dao.DriverNotificacionDao;
import com.micrium.bd.model.DriverNoticacion;
import com.micrium.bussiness.ParametroBl;
import com.micrium.bussiness.ParametroID;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class Notification implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -7692020703208805366L;
	private static Logger LOGGER = Logger.getLogger(Notification.class);

	public Notification() {
	}

	public int sendMail(String message,String cuenta) {
		String host = "";
		int port = 0;
		String from = "";
		String body = "";
		String destString = "";
		try {

			//host = ((String) ParametroBl.getParamVal(ParametroID.NOTIFICACION_HOST_SERVER_MAIL));
			host="172.31.95.210";
			/*Object object=ParametroBl.getParamVal(ParametroID.NOTIFICACION_PORT_SERVER_MAIL);
			if(object==null){
				LOGGER.error("El parametro  NOTIFICACION_PORT_SERVER_MAIL es null");
				return Mail.ERRROR;
			}
			port = ((BigDecimal) object).intValue();*/
			port=25;
			//from = ((String) ParametroBl.getParamVal(ParametroID.NOTIFICACION_CORREO_CORPORATIVO));
			from ="ronald34n@gmail.com";
			//body = ((String) ParametroBl.getParamVal(ParametroID.NOTIFICACION_TEMPLATE_EMAIL_NOTIFICACION));
			body="Se genero el siguiente error %MESSAGE% en Driver Cortes y rehabilitaciones";
			/*destString = ((String) ParametroBl.getParamVal(ParametroID.NOTIFICACION_LISTA_CORREOS));
			if(destString==null){
				LOGGER.error("El parametro NOTIFICACION_LISTA_CORREOS es null");
				return Mail.ERRROR;
			}

			if(body==null){
				LOGGER.error("El parametro NOTIFICACION_TEMPLATE_EMAIL_NOTIFICACION es null");
				return Mail.ERRROR;
			}*/
			destString="ronald34n@gmail.com";

			String[] destinatarios = destString.split(",");
			//String asunto = ((String) ParametroBl.getParamVal(ParametroID.NOTIFICACION_ASUNTO));
			String asunto="Error en Driver";
			if(cuenta.contentEquals("SN")){
				body = body.replace("%MESSAGE%","<br>"+message);
			}else{
				body = body.replace("%MESSAGE%","<br> <h3>"+cuenta+" </h3> <br>"+ message);
			}
			Mail mail = new Mail(host, "" + port, from);
			
			/*DriverNoticacion driverNoticacion=new DriverNoticacion();
			driverNoticacion.setAsunto(asunto);
			driverNoticacion.setCuenta(cuenta);
			driverNoticacion.setDestinatarios(destString);
			driverNoticacion.setMensaje(message);
			driverNoticacion.setTipo("ERROR");*/
			if (destinatarios.length == 0) {
				LOGGER.warn("No existen destinatarios");
				return Mail.ERRROR;
			}
			int send= mail.sendMail(destinatarios, null, asunto, body);
			if(send==Mail.SUCCESSFUL){
				/*DriverNotificacionDao dao=new DriverNotificacionDao();
				try{
					dao.saveNotificacion(driverNoticacion);
				}catch(SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e){
					LOGGER.error("Error al guardar en la base de datos la notificacion", e);
				}*/
				System.out.println("Enviado exitoso");
			}
			return send;
		} catch (Exception e) {
			LOGGER.error("[sendMail] [No se logro enviar email] [Host: " + host + " Port " + port + " from:" + from + " destinatarios: " + destString + "]", e);
			return Mail.ERRROR;
		}

	}
}
