package com.micrium.notificacion;
import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
public class Mail implements Serializable{
	private static final long serialVersionUID = -251512612688182574L;
	public static final int SUCCESSFUL=1;
	public static final int ERRROR=-1;
	private static Logger LOG = Logger.getLogger(Mail.class);
    private String from;
    private List<String> adjunto;
   
    private transient MimeMultipart multipart;
    private Properties props;
    
    public Mail(String host,String port,String from,boolean auth) {
        props = System.getProperties();
//        props.put("mail.smtp.starttls.enalable", "true");
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.user", from);
        if(auth)
        	props.setProperty("mail.smtp.auth", "true");
        else
        	props.setProperty("mail.smtp.auth", "false");
        
        multipart=new MimeMultipart("related");
    }
    public Mail(String host,String port,String from) {
    	this.from=from;
        props = System.getProperties();
        props.setProperty("mail.smtp.starttls.enalable", "false");
        props.setProperty("mail.smtp.host", host);
        props.setProperty("mail.smtp.port", port);
        props.setProperty("mail.smtp.user", from);
        props.setProperty("mail.smtp.auth", "false");
        multipart=new MimeMultipart("related");
        LOG.info("[Parametros Servidor] [Host:"+host +" port: "+port+" from: "+from);
    }
    public Mail(){
    	props = System.getProperties();
    	props.put("mail.smtp.auth", "false");
        props.put("mail.smtp.starttls.enable", "true");
    	props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.port", "25");
        //props.setProperty("mail.smtp.user","rllayus@gmail.com" );
        props.setProperty("mail.smtp.user","ronald34n@gmail.com" );
        multipart=new MimeMultipart("related");
    }
    /**
     * 
     * @param destinatario
     * @param copia
     * @param asunto
     * @param mensaje en 
     * @throws NoSuchProviderException
     * @throws MessagingException
     * @throws Exception
     */
    public int sendMail(String[] destinatario,String[] copia,String asunto,String mensaje) throws NoSuchProviderException, MessagingException, Exception {
    	
        Session mailSession = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(mailSession);
        message.setSubject(asunto);
        message.setFrom(new InternetAddress(from));
        Transport transport = mailSession.getTransport("smtp");
        InternetAddress addresses[] = new InternetAddress[destinatario.length];
        for (int i = 0; i < addresses.length; i++) {
            addresses[i] = new InternetAddress(destinatario[i]);
            
        }
      
        if (adjunto != null) {
            for (String adjunt : adjunto) {
                addAttach(adjunt); //ruta donde se encuentra el fichero que queremos adjuntar.
            }
        }
        if(asunto!=null&&!asunto.isEmpty())
        	message.setSubject(asunto);
        
        addContent(mensaje);
        
        message.addRecipients(Message.RecipientType.TO, addresses);
        message.setContent(multipart);
        transport.connect();
        transport.sendMessage(message,message.getRecipients(Message.RecipientType.TO));
        transport.close();
        LOG.info("[sendMail] [se envio emails]");
        return SUCCESSFUL;
        
    }

    /**
     * Lista de rutas de los archivos adjuntos
     */
    public List<String> getAdjunto() {
        return adjunto;
    }

    /**
     * Lista de rutas de los archivos adjuntos
     *
     * @param newVal
     */
    public void setAdjunto(List<String> newVal) {
        adjunto = newVal;
    }

 

    public String getUserMail() {
        return from;
    }

    /**
     *
     * @param newVal
     */
    public void setUserMail(String newVal) {
        from = newVal;
    }

    public Message readAllMail() {
        return null;
    }


    public MimeMultipart getMultiPart() {
        return multipart;
    }

    /**
     *
     * @param newVal
     */
    public void setMultiPart(MimeMultipart newVal) {
        multipart = newVal;
    }

    public void addContent(String htmlText) throws Exception {
        // first part (the html)
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(htmlText, "text/html");
        // add it
        this.multipart.addBodyPart(messageBodyPart);
    }

    public void addCID(String cidname, String pathname) throws Exception {
        DataSource fds = new URLDataSource(Mail.class.getResource(pathname));
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(fds));
        messageBodyPart.setHeader("Content-ID", "<" + cidname + ">");
        this.multipart.addBodyPart(messageBodyPart);
    }

    public void addAttach(String pathname) throws Exception {
        File file = new File(pathname);
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource ds = new FileDataSource(file);
        messageBodyPart.setDataHandler(new DataHandler(ds));
        messageBodyPart.setFileName(file.getName());
        messageBodyPart.setDisposition(Part.ATTACHMENT);
        this.multipart.addBodyPart(messageBodyPart);
    }
}

