/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.drivercr;

import com.micrium.bussiness.JobEjecutorComando;
import com.micrium.bussiness.LogTransaccion;
import com.micrium.bussiness.ParametroBl;
import com.micrium.bussiness.ParametroID;
import com.micrium.bussiness.PlanificadorEjecucion;
import com.micrium.jms.QueueReceive;
import com.micrium.jms.TopicReceive;
import java.text.ParseException;
import java.util.TimeZone;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverCr {

	private static final Logger LOGGER = Logger.getLogger(DriverCr.class);

	public static void main(String[] args) {
		
		System.out.println(System.getProperty("user.name"));
		// TODO code application logic here
		PropertyConfigurator.configure("etc/log4j.properties");
		TimeZone.setDefault(TimeZone.getTimeZone("America/La_Paz"));
		String jmsType = (String) ParametroBl.getParamVal(ParametroID.JMS_TYPE);
		if (jmsType == null || jmsType.isEmpty()) {
			LOGGER.error("No se logro obtener el parametro JMS_TYPE");
			LogTransaccion.error("No se logro obtener el parametro JMS_TYPE", "SN");
			return;
		}

		//============================= INICIADOR DE CLIENTE DE COLA ==================
		LOGGER.info("*********** INICIANDO DRIVER  V 1.0 ... ***********");

		try {
			if (jmsType.trim().equalsIgnoreCase("queue")) {
				LOGGER.info("[Se inicia conexion tipo Queue]");
				new QueueReceive();
			} else {
				LOGGER.info("[Se inicia conexion tipo Topic]");
				new TopicReceive();
			}
			LOGGER.info("Conexion al servidor de colas exitoso");
		} catch (NamingException | JMSException e) {
			LOGGER.error("No logro iniciar la conexion hacia el sistema de cola", e);
			LogTransaccion.error("No logro iniciar la conexion hacia el sistema de cola\n" + e.getMessage(), "SN");
		}

		LOGGER.info("*********** INICIANDO DEMONIO PLANIFICADOR DE EJECUCION ***********");
		PlanificadorEjecucion pe = new PlanificadorEjecucion();
		pe.start();

		try {
			SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
			Scheduler sched = schedFact.getScheduler();
			sched.start();

			//======================= CRON EJECTUTOR COMANDO ===============================
			try {
				JobDetail ejecutorComando = new JobDetail("ExecutorComand", Scheduler.DEFAULT_GROUP,
						JobEjecutorComando.class);
				CronTrigger ejecutorComandoTrigger = new CronTrigger("cronExecutorComand", Scheduler.DEFAULT_GROUP,
						(String) ParametroBl.getParamVal(ParametroID.CRON_EJECUTOR_COMANDO_EXPRESION));
				sched.scheduleJob(ejecutorComando, ejecutorComandoTrigger);
			} catch (SchedulerException | ParseException e) {
				LOGGER.error("No se logro inicializar el cron EJECUTOR COMANDO", e);
				LogTransaccion.error("No se logro inicializar el cron EJECUTOR COMANDO\n" + e.getMessage(), "SN");
			}
			//===============================================================================
			
		} catch (SchedulerException ex) {
			LOGGER.error("Error al iniciar los procesos CRON", ex);
			LogTransaccion.error("Error al iniciar los procesos CRON\n" + ex.getMessage(), "SN");
		}
	}

}
