/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bussiness;

import com.micrium.bd.dao.DriverLogTransaccionDao;
import com.micrium.bd.model.DriverLogTransaccion;
import com.micrium.notificacion.Mail;
import com.micrium.notificacion.Notification;
import java.sql.SQLException;

/**
 *
 * @author Ricardo Laredo
 */
public class LogTransaccion {
	private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(LogTransaccion.class);
	private static final String LEVEL_ERROR="ERROR";
	private static final String LEVEL_INFO="INFO";
	private static DriverLogTransaccionDao transaccionDao=new DriverLogTransaccionDao();
	public static void error(String accion,String cuenta){
		DriverLogTransaccion log=new DriverLogTransaccion();
		log.setAccion(accion);
		log.setTipo(LEVEL_ERROR);
		log.setUsuario("DRIVER");
		log.setCuenta(cuenta);
		try {
			transaccionDao.saveLogTransaccion(log);
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			LOGGER.error("No se logro registrar el log en la base de datos",ex);
		}
		
		Notification notification=new Notification();
		if(notification.sendMail(accion,cuenta)==Mail.SUCCESSFUL){
			LOGGER.info("Notificacion de error enviado correctamente");
		}
		
		
	}
	public static void info(String accion,String cuenta){
	DriverLogTransaccion log=new DriverLogTransaccion();
		log.setAccion(accion);
		log.setTipo(LEVEL_INFO);
		log.setUsuario("DRIVER");
		log.setCuenta(cuenta);
		try {
			transaccionDao.saveLogTransaccion(log);
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			LOGGER.error("No se logro registrar el log en la base de datos",ex);
		}
	}
	
	
}
