/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bussiness;

/**
 *
 * @author Ricardo Laredo
 */
public class ParametroID {

	public static final short MSC_IP = 100;
	public static final short MSC_PUERTO = 101;
	public static final short MSC_USUARIO = 102;
	public static final short MSC_PASSWORD = 103;
	public static final short COMANDO_CANTIDAD_REINTENTO = 104;
	public static final short MSC_HABILITADO = 105;

	/**
	 * Parametros para conexion a WS
	 */
	public static final short JMS_TYPE = 120;
	public static final short INITIAL_CONTEXT_FACTORY = 121;
	public static final short JMS_REMOTE_CONNECTION_FACTORY = 122;
	public static final short NAME_QUEUE_TOPIC = 123;
	public static final short JBOSS_APPLICATION_USER = 124;
	public static final short JBOSS_APPLICATION_PASSWORD = 125;
	public static final short REMOTE_URL = 126;
	public static final short SEPARADOR_TRAMA=127;
	/**
	 * Parametros de cron
	 */
	public static final short CRON_EJECUTOR_COMANDO_EXPRESION = 130;
	public static final short ACTUALIZA_PARAMETRO_CRON_EXPRESION = 131;

	public static final short TITULO_APLICACION = 22;
	public static final short PIE_PAGINA = 23;

	/**
	 * Parametros de validaciones
	 */
	public static final short RANGO_INICIO_TELEFONIA_FIJA = 160;
	public static final short RANGO_FIN_TELEFONIA_FIJA = 161;
	public static final short RANGO_INICIO_PLAN_800 = 162;
	public static final short RANGO_FIN_PLAN_800 = 163;
	public static final short NOMBRE_PROCESO_DRIVER = 164;
	

	/**
	 * Notificacion
	 */

	public static final short NOTIFICACION_ASUNTO = 155;
	public static final short NOTIFICACION_LISTA_CORREOS = 154;
	public static final short NOTIFICACION_TEMPLATE_EMAIL_NOTIFICACION = 153;
	public static final short NOTIFICACION_CORREO_CORPORATIVO = 152;
	public static final short NOTIFICACION_PORT_SERVER_MAIL = 151;
	public static final short NOTIFICACION_HOST_SERVER_MAIL = 150;

}
