/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bussiness;

import com.micrium.bd.dao.DriverComandoDao;
import com.micrium.bd.dao.DriverListDao;
import com.micrium.bd.model.DriverComando;
import com.micrium.bd.model.DriverLstCldpreana;
import com.micrium.bd.model.DriverLstCltdsg;
import com.micrium.telnet.ClienteMsc;
import com.micrium.telnet.ClienteSSL;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 *
 * @author Ricardo Laredo
 *
 * Envia un comando cada cierto tiempo comando para recivir un lista como
 * respuesta que debe ser almacenado en base de datos
 */
public class JobEjecutorComando implements Job {

	private static final String COMANDO_LISTA = "COMANDO_LISTA";
	private static final String TABLE_LST_CLRDSG = "LST CLRDSG";

	private static final Logger LOGGER = Logger.getLogger(JobEjecutorComando.class);
	private ClienteMsc clienteTelnet;
	private DriverComandoDao comandoDao;
	private DriverListDao listDao;

	@Override
	public void execute(JobExecutionContext jec) throws JobExecutionException {
		String pront;
		try {
			clienteTelnet = new ClienteSSL();
			LOGGER.info("[PROCESO CRON] Conectado correctamente al servidor MSC");
		} catch (IOException ex) {
			LOGGER.error("[PROCESO CRON] Error al conectar con el servidor MSC", ex);
			LogTransaccion.error("[PROCESO CRON] Error al conectar con el servidor MSC\n" + ex.getMessage(), "SN");
			return;
		} catch (Exception ex) {
			LOGGER.error("[PROCESO CRON] Error al conectar con el servidor MSC posiblemente porque no se logro obtener los parametros IP, PUERTP", ex);
			LogTransaccion.error("[PROCESO CRON] Error al conectar con el servidor MSC posiblemente porque no se logro obtener los parametros IP, PUERTP\n" + ex.getMessage(), "SN");
			return;
		}

		try {
			pront = clienteTelnet.login();
			if (pront != null && !pront.contains(ClienteSSL.PRONT_LOGIN)) {
				LOGGER.error("Credenciales incorrectos al iniciar sesion: " + pront);
				LogTransaccion.error("Credenciales incorrectos al iniciar sesion: " + pront, "SN");
				return;
			}
			LOGGER.info("[PROCESO CRON] Sesion iniciada correctamente en servidor MSC>" + pront);
		} catch (IOException ex) {
			LOGGER.error("[PROCESO CRON]Error al iniciar sesion en el servidor MSC", ex);
			LogTransaccion.error("[PROCESO CRON] Error al iniciar sesion en el servidor MSC\n" + ex.getMessage(), "SN");
			return;
		} catch (Exception ex) {
			LOGGER.error("[PROCESO CRON] Error al iniciar sesion en el servidor MSC, posiblemente porque no se logro obtener los parametros USUARIO,PASSWORD", ex);
			LogTransaccion.error("[PROCESO CRON] Error al iniciar sesion en el servidor MSC, posiblemente porque no se logro obtener los parametros USUARIO,PASSWORD\n" + ex.getMessage(), "SN");
			return;
		}

		try {
			LOGGER.info("[PROCESO CRON] Ejecutando comando para habilitar la ejecucion de comandos: " + ClienteSSL.COMAND_INIT);
			pront = clienteTelnet.sendComandInit();
			if (pront != null && !pront.contains(ClienteSSL.PRONT_INIT_COMAND_SUCCESS)) {
				LOGGER.error("[PROCESO CRON] Error al ejecutar comando para habilitar ejecucion de comandos: \n" + pront);
				LogTransaccion.error("[PROCESO CRON] Error al ejecutar comando para habilitar ejecucion de comandos: " + pront, "SN");
				return;
			}
		} catch (IOException ex) {
			LOGGER.error("[PROCESO CRON] Error al ejecutar comando para habilitar ejecucion de comandos", ex);
			LogTransaccion.error("[PROCESO CRON] Error al ejecutar comando para habilitar ejecucion de comandos\n" + ex.getMessage(), "SN");
			return;
		} catch (Exception ex) {
			LOGGER.error("[PROCESO CRON] Error al ejecutar comando para habilitar ejecucion de comandos", ex);
			LogTransaccion.error("[PROCESO CRON] Error al ejecutar comando para habilitar ejecucion de comandos\n" + ex.getMessage(), "SN");
			return;
		}

		this.comandoDao = new DriverComandoDao();
		List<DriverComando> comandos = null;
		try {
			comandos = this.comandoDao.obtenerComando(COMANDO_LISTA, "AC");
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			LOGGER.error("[PROCESO CRON] No se logro obtener la lista de comandos a ejecutar", ex);
			LogTransaccion.error("[PROCESO CRON] No se logro obtener la lista de comandos a ejecutar\n" + ex.getMessage(), "SN");
			this.clienteTelnet.close();
			return;
		}

		if (comandos == null) {
			LOGGER.error("[PROCESO CRON] Lista de comandos es null");
			LogTransaccion.error("[PROCESO CRON] Lista de comandos es null", "SN");
			this.clienteTelnet.close();
			return;
		}

		if (comandos.isEmpty()) {
			LOGGER.error("[PROCESO CRON] No existen comandos para ser ejecutados");
			LogTransaccion.error("[PROCESO CRON] No existen comandos para ser ejecutados", "SN");
			this.clienteTelnet.close();
			return;
		}
		LogTransaccion.info("[PROCESO CRON] Inicio de ejecucion de comando LST", "SN");
		for (DriverComando comando : comandos) {
			try {
				clienteTelnet.sendComandoToList(comando.getComando());
			} catch (IOException ex) {
				LOGGER.error("[PROCESO CRON] Error al ejecutar comando: " + comando, ex);
				LogTransaccion.error("[PROCESO CRON] Error al ejecutar comando: " + comando + "\n" + ex.getMessage(), "SN");
				this.clienteTelnet.close();
				return;
			}

			try {
				String linea;
				while ((linea = clienteTelnet.readLine()) != null) {
					List<String> list = parce(linea);
					if (list.size() == 10) {
						if (comando.getComando().contains(TABLE_LST_CLRDSG)) {
							saveOnTableLstCltdsg(list);
						} else {
							saveOnTableLstCldpreana(list);
						}
					}

					if (linea.contains("END")) {
						break;
					}

				}
			} catch (IOException ex) {
				LOGGER.error("[PROCESO CRON] Error al realizar la lecturas de la lista", ex);
				LogTransaccion.error("[PROCESO CRON] Error al leer datos del servidor MSC:\n " + ex.getMessage(), "SN");
			}
		}
		LOGGER.info("[PROCESO CRON] Finalizacion de ejecucion de comandos diarios");
		LogTransaccion.info("[PROCESO CRON] Ejecucion de comandos LST exitoso", "SN");
		clienteTelnet.close();
	}

	/**
	 * Comvierte Una linea leida del servidor MSC a columnas adecuadas
	 *
	 * @param string Linea leida del servidor
	 * @return Una lista con los campos parceados
	 */
	private List<String> parce(String string) {
		List<String> list = new ArrayList<>();
		StringBuilder palabra = new StringBuilder();
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) != ' ') {
				palabra.append(string.charAt(i));
			} else {
				if (i + 1 < string.length() && string.charAt(i + 1) != ' ') {
					palabra.append(string.charAt(i));
				} else {
					if (palabra.length() > 0) {
						list.add(palabra.toString().trim());
						palabra = new StringBuilder();
					}
				}

			}
		}

		return list;
	}

	private void saveOnTableLstCltdsg(List<String> list) {
		try {
			listDao = new DriverListDao();
			DriverLstCltdsg cltdsg = new DriverLstCltdsg();
			cltdsg.setGroupNumber(Integer.parseInt(list.get(0)));
			cltdsg.setCallerNumber(list.get(1));
			cltdsg.setAddressNature(list.get(2));
			cltdsg.setFunctionCode(list.get(3));
			cltdsg.setMinimunNumberLength(Integer.parseInt(list.get(4)));
			cltdsg.setMaximunNumberLength(Integer.parseInt(list.get(5)));
			cltdsg.setCallSourceName(list.get(6));
			cltdsg.setChargingSourceCode(Integer.parseInt(list.get(7)));
			cltdsg.setBarringGroupNumber(Integer.parseInt(list.get(8)));
			cltdsg.setCallerNumberChangeName(list.get(9));
			this.listDao.saveLstCltdsg(cltdsg);
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			LOGGER.error("[PROCESO CRON]  Error al guardar la lista <LstCltdsg>", ex);
			LogTransaccion.error("[PROCESO CRON]  Error al guardar la lista <LstCltdsg>\n " + ex.getMessage(), "SN");
		}

	}

	private void saveOnTableLstCldpreana(List<String> list) {
		try {
			listDao = new DriverListDao();
			DriverLstCldpreana cldpreana = new DriverLstCldpreana();
			cldpreana.setCallSourceName(list.get(0));
			cldpreana.setCallOriginator(list.get(1));
			cldpreana.setCallPrefix(list.get(2));
			cldpreana.setCallerNumberMinimunLength(list.get(3));
			cldpreana.setCallerNumberAddressNature(list.get(4));
			cldpreana.setCallerRoamingType(list.get(5));
			cldpreana.setCalledNumberChangeName(list.get(6));
			cldpreana.setNewDnSet(list.get(7));
			cldpreana.setManagedObjectGroup(list.get(8));
			cldpreana.setServerName(list.get(9));
			this.listDao.saveLstCldpreana(cldpreana);
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			LOGGER.error("[PROCESO CRON] Error al guardar la lista <LstCldpreana>", ex);
			LogTransaccion.error("[PROCESO CRON]  Error al guardar la lista <LstCldpreana>\n " + ex.getMessage(), "SN");
		}
	}

}
