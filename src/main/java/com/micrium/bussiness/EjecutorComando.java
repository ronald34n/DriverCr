package com.micrium.bussiness;

import com.micrium.bd.dao.DriverColaTramaDao;
import com.micrium.bd.dao.DriverComandoDao;
import com.micrium.bd.dao.DriverEjecucionComandoDao;
import com.micrium.bd.dao.DriverRangoTelefoniaDao;
import com.micrium.bd.model.DriverColaTrama;
import com.micrium.bd.model.DriverComando;
import com.micrium.bd.model.DriverEjecucionComando;
import com.micrium.telnet.ClienteMsc;
import com.micrium.telnet.ClienteSSL;
import com.micrium.utils.Utils;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import micrium.eventos_bccs.modelo.Cuenta;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class EjecutorComando extends Thread {
    private static final String TELEFONIA_FIJA = "TELEFONIA_FIJA";
    private static final String PLAN_800 = "PLAN_800";
    private static final String NUMERACION_MOVIL="NUMERACION_MOVIL";

    private static final Logger LOGGER = Logger.getLogger(EjecutorComando.class);
    private IEjecutorComandoStop listener;
    private DriverColaTramaDao colaTramaDao;
    private ClienteMsc clienteTelnet;
    private DriverEjecucionComandoDao ejecucionComandoDao;
    private DriverComandoDao comandoDao;
    private DriverRangoTelefoniaDao rangoTelefoniaDao;

    public EjecutorComando(IEjecutorComandoStop listener) throws IOException, Exception {
        super();
        this.listener = listener;
        colaTramaDao = new DriverColaTramaDao();
        clienteTelnet = new ClienteSSL();
        this.ejecucionComandoDao = new DriverEjecucionComandoDao();
        this.comandoDao = new DriverComandoDao();
        this.rangoTelefoniaDao = new DriverRangoTelefoniaDao();
    }

    @Override
    public void run() {
        try {
            LOGGER.info("Ejecutor " + getId() + " de comando Iniciado");

            DriverColaTrama colaTrama;
            LOGGER.info("Iniciando sesion en Servidor MSC");
            String pront = clienteTelnet.login();
            if (pront != null && !pront.contains(ClienteSSL.PRONT_LOGIN)) {
                LOGGER.error("Credenciales incorrectos al iniciar sesion: " + pront);
                LogTransaccion.error("Credenciales incorrectos al iniciar sesion: " + pront, "SN");
                this.clienteTelnet.close();
                return;
            }

            LOGGER.info("Inicio de sesion correcto en el servidor:  " + pront);
            LOGGER.info("Ejecutando comando para habilitar la ejecucion de comandos: " + ClienteSSL.COMAND_INIT);
            pront = clienteTelnet.sendComandInit();
            if (pront != null && !pront.contains(ClienteSSL.PRONT_INIT_COMAND_SUCCESS)) {
                LOGGER.error("Error al ejecutar comando para habilitar ejecucion de comandos: " + pront);
                LogTransaccion.error("Error al ejecutar comando para habilitar ejecucion de comandos: " + pront, "SN");
                this.clienteTelnet.close();
                return;
            }

            try {
                while ((colaTrama = colaTramaDao.obtenerTrama()) != null) {
                    LOGGER.info("Se obtuvo un elemento de la cola exitosamente");
                    LOGGER.info("Obteniendo los comandos ejecutados para esta trama: " + colaTrama.getTramaBccs());
                    Cuenta cuenta = Utils.parceTrama(colaTrama.getTramaBccs());
                    int numero = Integer.valueOf(cuenta.getIMSI());
                    String plan = validarRango(numero);
                    LOGGER.info("Plan: " + plan);
                    List<DriverComando> comandos;
                    boolean updateEjecucionComando = false;
                    DriverEjecucionComando ejecucionComando = ejecucionComandoDao.getComandoEjecucion(colaTrama.getTramaBccs());
                    if (ejecucionComando != null) {
                        updateEjecucionComando = true;
                        LOGGER.info("Ejecutando reintento manual de comando");
                        ejecucionComando.setEstado(DriverEjecucionComando.ESTADO_OK);
                        if (ejecucionComando.getComandoMscFallido() == null || ejecucionComando.getComandoMscFallido().isEmpty()) {
                            LOGGER.error("No se logro obtener los comandos de error");
                            continue;
                        }
                        String arrayComandos[] = ejecucionComando.getComandoMscFallido().split("###");
                        comandos = new ArrayList<>();
                        for (String arrayComando : arrayComandos) {
                            LOGGER.info("Comandoa-->" + arrayComando);
                            DriverComando comando = new DriverComando();
                            comando.setComando(arrayComando);
                            comandos.add(comando);
                        }

                    } else {
                        ejecucionComando = new DriverEjecucionComando();
                        ejecucionComando.setCuenta(colaTrama.getCuenta());
                        ejecucionComando.setTramaBccs(colaTrama.getTramaBccs());
                        ejecucionComando.setUsuario(colaTrama.getUsuario());
                        ejecucionComando.setEstado(DriverEjecucionComando.ESTADO_OK);
                        comandos = this.comandoDao.obtenerComando(plan, cuenta.getOtros());
                        if (comandos == null) {
                            LOGGER.error("Error al obtener lista de comandos, retorno null");
                            LogTransaccion.error("Error al obtener lista de comandos, retorno null", "SN");
                            continue;
                        }
                    }

                    if (comandos.isEmpty()) {
                        LOGGER.error("No existen comandos para ejecutar para esta operacion");
                        LogTransaccion.error("No existen comandos para ejecutar para esta operacion:", colaTrama.getCuenta());
                        continue;
                    }

                    try {
                        LOGGER.info("Cantidad de comandos encontrados para ejecutar: " + comandos.size());
                        boolean comandosEjecutados = true;
                        StringBuilder comandoMsc = new StringBuilder();
                        StringBuilder comandoMscFallidos = new StringBuilder();
                        for (DriverComando comando : comandos) {
                            LOGGER.info("Cola Trama: " + colaTrama.getCuenta());
                            String cdo = comando.getComando().replace("%CUENTA%", colaTrama.getCuenta());
                            //@ronald aqui se va modificar para caso de 3 tipo plan
                            if (plan.equals(PLAN_800)) {
                                cdo = cdo.replace("%LENGTH%", "9");
                            } else {
                            	if (plan.equals(TELEFONIA_FIJA)){
                                    cdo = cdo.replace("%LENGTH%", "8");
                            	}else{
                            		cdo=cdo.replaceAll("%LENGTH%", "8");//aqui seria para NUMERACION_MOVIL
                            	}
                            }

                            LOGGER.info("Ejecutanto comando -->: " + cdo);
                            // if (comandosEjecutados) {
                            pront = clienteTelnet.sendComando(cdo);
                            if (pront.contains(ClienteSSL.PRONT_COMANDO_SUCCESS)) {
                                comandoMsc.append(cdo);
                                comandoMsc.append("  -->OK");
                            } else {
                                comandosEjecutados = false;
                                LOGGER.error("El comando no se ejecuto correctante: " + pront);
                                comandoMsc.append(cdo);
                                comandoMsc.append("  -->ERROR");
                                comandoMscFallidos.append(cdo);
                            }
                            //} else {
                            //  comandoMsc.append(cdo);
                            //comandoMsc.append("  -->ERROR");
                            // comandoMscFallidos.append("###");
                            // comandoMscFallidos.append(cdo);
                            // }
                            comandoMsc.append("\n");
                        }

                        ejecucionComando.setComandoMsc(comandoMsc.toString());
                        if (comandosEjecutados) {
                            ejecucionComando.setMotivoFallo("");
                            //aqui modificar para el requisito mensajes amigables  
                            ejecucionComando.setComandoMscFallido("");     
                            LOGGER.info("[" + colaTrama.getCuenta() + "]Comando ejecutado correctamente");
                            LogTransaccion.info("Comando ejecutado correctamente :" + ClienteSSL.PRONT_COMANDO_SUCCESS, colaTrama.getCuenta());
                        } else {
                            LogTransaccion.error("No se ejecuto el comando correctamente: " + pront, colaTrama.getCuenta());
                            ejecucionComando.setEstado(DriverEjecucionComando.ESTADO_ERROR);
                            if (comandos.size() > 0) {
                                LOGGER.error("El servidor MSC devolvio lo siguiente: " + pront);
                                if (pront.indexOf("RETCODE = 0")==-1){ //se añadio este if para el primer caso 
                                	if (pront.isEmpty()){
                                		ejecucionComando.setMotivoFallo(pront);
                                	}else{
                                		int aux=pront.indexOf("RETCODE = 0");
                                		int aux2=pront.indexOf("(Number of results");
                                		ejecucionComando.setMotivoFallo((aux>0 && aux2>0)?pront.substring(0,aux+13)+pront.substring(aux2,pront.length()):pront );
                                		/*if (aux>0 && aux2>0){
                                			ejecucionComando.setMotivoFallo(pront.substring(0,aux+13)+pront.substring(aux2,pront.length()));
                                		}
                                		ejecucionComando.setMotivoFallo(pront);*/
                                	}
                                }
                                //ejecucionComando.setMotivoFallo("El servidor MSC devolvio lo siguiente: " + pront);
                                ejecucionComando.setComandoMscFallido(comandoMscFallidos.toString());
                            } else {
                                ejecucionComando.setMotivoFallo("No existen comandos para ejecutar para el PLAN :" + plan + ", ESTADO BCCS : " + cuenta.getOtros());
                            }
                        }
                    } catch (IOException ex) {
                        ejecucionComando.setEstado(DriverEjecucionComando.ESTADO_ERROR);
                        ejecucionComando.setMotivoFallo(ex.getMessage());
                        LOGGER.error("No se logro ejecutar el comando o exitio una excepcion al leer datos del socket", ex);
                        LogTransaccion.error("No se logro ejecutar el comando o exitio una excepcion al leer datos del socket\n" + ex.getMessage(), colaTrama.getCuenta());
                    }

                    try {
                    	//aqui se va modificar para el tercer caso, cuando viene el PK(PRE ELIMINADO) en la web aparece pk y ac, solo tiene que mostrar pk
                    	//buscar el pk que ya se inserto para no insertar el AC añadir buscar
                    	
                    	
                    	
                        if (!updateEjecucionComando) {
                            ejecucionComandoDao.saveComando(ejecucionComando);
                            LOGGER.info("Trama guardado correctamente en tabla DriverEjecucionComando");
                        } else {
                            ejecucionComandoDao.updateComando(ejecucionComando);
                            LOGGER.info("Trama actualizado correctamente en la tabla DriverEjecucionComando");
                        }

                    } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                        LOGGER.error("No se logro regitrar la trama en la tabla DriverEjecucionComando", ex);
                        LogTransaccion.error("No se logro regitrar la trama en la tabla DriverEjecucionComando\n" + ex.getMessage() + "\n" + colaTrama.getTramaBccs(), colaTrama.getCuenta());

                    }
                }
                LOGGER.info("Ya no existen mas elementos en la cola");
            } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
                LOGGER.error("No se logro obtener un elemento de la cola de la base de datos", ex);
                LogTransaccion.error("No se logro obtener un elemento de la cola de la base de datos\n" + ex.getMessage(), "SN");
            }

        } catch (IOException ex) {
            LOGGER.error("Error al realizar login o iniciar comando de ejecucion de comandos", ex);
            LogTransaccion.error("Error al realizar login o iniciar comando de ejecucion de comandos", "SN");
        } catch (Exception ex) {
            LOGGER.error("Error al realizar login o iniciar comando de ejecucion de comandos", ex);
            LogTransaccion.error("Error al realizar login o iniciar comando de ejecucion de comandos", "SN");
        } finally {
            if (listener != null) {
                listener.stop(getId());
            }
            this.clienteTelnet.close();
        }
    }

    private String validarRango(int numero) {

        try {
            return this.rangoTelefoniaDao.obtenerPlanComercial(numero);
        } catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
            LOGGER.error("No se logro validar el rango para obtener el plan comercia del numero: " + numero, ex);
        }
        return "No logro validar el rango";
    }
}
