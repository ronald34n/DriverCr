/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bussiness;

import com.micrium.bd.dao.DriverColaTramaDao;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class PlanificadorEjecucion extends Thread implements IEjecutorComandoStop {

	private static final Logger LOGGER = Logger.getLogger(PlanificadorEjecucion.class);

	private static final int CANTIDAD_MAXIMA_EJECUTORES = 1;
	private static final int RAZON_ELEMENTOS_COLA = 1000;
	public static AtomicBoolean PLANING = new AtomicBoolean(true);
	private static Map<Long, EjecutorComando> listEjecutores = new HashMap();
	private DriverColaTramaDao colaTramaDao;

	public PlanificadorEjecucion() {
		super();
		this.colaTramaDao = new DriverColaTramaDao();

	}

	@Override
	public void run() {
		while (true) {
			synchronized (listEjecutores) {
				int cantidadElementosCola = 0;
				try {
					cantidadElementosCola = colaTramaDao.contarCantidadEnCola();
				} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
					LOGGER.error("Error al obtener la cantidad de tramas encolados en la base de datos", ex);
					LogTransaccion.error("Error al obtener la cantidad de tramas encolados en la base de datos\n" + ex.getMessage(), "SN");
				}

				//Planificador en modo de espera porque no hay elementos en la cola
				LOGGER.info("Cantidad elementos en la Cola: " + cantidadElementosCola);
				while (cantidadElementosCola == 0) {
					try {
						LOGGER.info("Planificador en modo de espera porque no hay elementos en la cola");
						listEjecutores.wait();
						LOGGER.info("Planificador en ejecucion despues de esperar por no existencia de elementos en la cola");
					} catch (InterruptedException ex) {
						LOGGER.error("Error al poner en modo de espera al Planificador de Ejecucion", ex);
						LogTransaccion.error("Error al poner en modo de espera al Planificador de Ejecucion \n" + ex.getMessage(), "SN");
					}

					try {
						cantidadElementosCola = colaTramaDao.contarCantidadEnCola();
					} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
						LOGGER.error("Error al obtener la cantidad de tramas encolados en la base de datos", ex);
						LogTransaccion.error("Error al obtener la cantidad de tramas encolados en la base de datos\n" + ex.getMessage(), "SN");
					}
				}

				int cantidadEjecutores = listEjecutores.size();
				//Planificador en modo de espera por cantidad maxima de ejecutores
				while (cantidadEjecutores >= CANTIDAD_MAXIMA_EJECUTORES) {
					try {
						LOGGER.info("Planificador en modo de espera por cantidad maxima de ejecutores");
						listEjecutores.wait();
						LOGGER.info("Planificador en ejecucion despues de esperar por cantidad maxima de ejecutores");
					} catch (InterruptedException ex) {
						LOGGER.error("Error al poner en modo de espera al Planificador de Ejecucion", ex);
						LogTransaccion.error("Error al poner en modo de espera al Planificador de Ejecucion \n" + ex.getMessage(), "SN");
					}
					cantidadEjecutores = listEjecutores.size();
				}

				LOGGER.info("CantidadEjecutores: " + cantidadEjecutores + "  CantidadElementosCola: " + cantidadElementosCola);
				while (cantidadEjecutores > 0 && cantidadElementosCola < RAZON_ELEMENTOS_COLA) {
					try {
						LOGGER.info("Planificador en modo de espera por cantidad de minima de elementos en la cola");
						listEjecutores.wait();
						LOGGER.info("Planificador en ejecucion despues de esperar por cantidad de minima de elementos en la cola");
					} catch (InterruptedException ex) {
						LOGGER.error("Error al poner en modo de espera al Planificador de Ejecucion", ex);
						LogTransaccion.error("Error al poner en modo de espera al Planificador de Ejecucion \n" + ex.getMessage(), "SN");
					}

					try {
						cantidadElementosCola = colaTramaDao.contarCantidadEnCola();
					} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
						LOGGER.error("Error al obtener la cantidad de tramas encolados en la base de datos", ex);
						LogTransaccion.error("Error al obtener la cantidad de tramas encolados en la base de datos\n" + ex.getMessage(), "SN");
					}
					cantidadEjecutores = listEjecutores.size();
				}

				boolean ejecutar = false;
				if (cantidadElementosCola > 0 && cantidadEjecutores == 0) {
					ejecutar = true;
				}

				if (cantidadElementosCola > RAZON_ELEMENTOS_COLA) {
					ejecutar = true;
				}

				if (cantidadEjecutores <= CANTIDAD_MAXIMA_EJECUTORES && ejecutar) {
					EjecutorComando ec;
					int cantidadMaximaIntentos = 3;
					Object object = ParametroBl.getParamVal(ParametroID.COMANDO_CANTIDAD_REINTENTO);
					if (object != null) {
						cantidadMaximaIntentos = ((BigDecimal) object).intValue();
					} else {
						LOGGER.warn("No se logro obtener el parametro cantidad maxima de reintentos");
					}
					boolean connected = false;
					int contadorIntentos = 0;
					LOGGER.info("Cantidad de reintentos en caso de error: " + cantidadMaximaIntentos);
					while (!connected && contadorIntentos < cantidadMaximaIntentos) {
						try {
							ec = new EjecutorComando(this);
							ec.start();
							connected = true;
							listEjecutores.put(ec.getId(), ec);
						} catch (IOException ex) {
							LOGGER.error("No logro iniciar el Hilo EjecutorComando posiblemente por que no se logro realizar la conexion con el servidor MSC", ex);
							LogTransaccion.error("No logro iniciar el Hilo EjecutorComando posiblemente por que no se logro realizar la conexion con el servidor MSC\n" + ex.getMessage(), "SN");
						} catch (Exception ex) {
							LOGGER.error("No logro iniciar el Hilo EjecutorComando posiblemente por que no se logro obtener los parametros de IP, PUERTO", ex);
							LogTransaccion.error("No logro iniciar el Hilo EjecutorComando posiblemente por logro obtener los parametros de IP, PUERTO\n" + ex.getMessage(), "SN");
						}
					}
					if (!connected && contadorIntentos >= cantidadMaximaIntentos) {
						LOGGER.error("No se logro conectar al servidor de MSC despues de " + cantidadMaximaIntentos + " intentos");
						LogTransaccion.error("No se logro conectar al servidor MSC despues de " + cantidadMaximaIntentos + " intentos", "SN");
					}
				}

			}

		}
	}

	@Override
	public void stop(long idThread) {
		synchronized (listEjecutores) {
			LOGGER.info("Ejecutor " + idThread + " detenido");
			listEjecutores.remove(idThread);
			listEjecutores.notifyAll();
		}
	}

	public static void notificar() {
		synchronized (listEjecutores) {
			if (listEjecutores.size() == 0) {
				listEjecutores.notifyAll();
			}
		}
	}
}
