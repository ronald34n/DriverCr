/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bussiness;


import com.micrium.bd.dao.ParametroDao;
import com.micrium.bd.model.MuParametro;
import java.util.HashMap;
import java.util.List;
import micrium.aes.AlgoritmoAES;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class ParametroBl {

	private static final Logger LOGGER = Logger.getLogger(ParametroBl.class);
	public static final int TIPO_CADENA = 1;
	public static final int TIPO_FECHA = 2;
	public static final int TIPO_NUMERICO = 3;
	public static final int TIPO_BOOLEANO = 4;
	public static final int TIPO_LISTA = 6;
	private static final HashMap<Long, MuParametro> parameters = new HashMap<>();
	private static ParametroDao parametroDao;
	private static AlgoritmoAES aes;
	static {
		try {
			aes=new AlgoritmoAES();
			parametroDao = new ParametroDao();
			if (parameters.isEmpty()) {
				cargarParametros();
			}
		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

	public static void updateParameters() {
		synchronized(parameters){
			parameters.clear();
			parameters.notify();
		}
		cargarParametros();
	}

	public ParametroBl() {
		
	}
	/**
	 * Obtiene los datos de la memoria
	 * @param idParametro
	 * @return 
	 */
	public static Object getParamVal(long idParametro) {
		synchronized (parameters) {
			if(parameters.isEmpty()){
				cargarParametros();
			}
			try {
				MuParametro p = parameters.get(idParametro);
				switch (p.getTipo()) {
					case TIPO_CADENA:
						return aes.desencriptar(p.getValorcadena());
					case TIPO_NUMERICO:
						return p.getValornumerico();
					case TIPO_LISTA:
						return aes.desencriptar(p.getValorcadena());
						
					case TIPO_BOOLEANO:
						return p.getValorbooleano();
					case TIPO_FECHA:
						return p.getValorfecha();
						
				}
				parameters.notify();
				return null;

			} catch (Exception e) {
				LOGGER.error("[Carga de Parametros] Error:" + e.getMessage(), e);
				parameters.notify();
				return null;
			}
		}

	}

	private static void cargarParametros() {
		LOGGER.info("Cargando parametros ...");
		synchronized (parameters) {
			try {
				List<MuParametro> list = parametroDao.getAllParameters();
				parameters.clear();
				for (MuParametro parametro : list) {
					parameters.put(parametro.getParametroId(), parametro);
				}
			} catch (Exception ex) {
				LOGGER.error(ex);
			}
			parameters.notify();
		}
		LOGGER.info("Se termino de cargar parametros");
	}

}
