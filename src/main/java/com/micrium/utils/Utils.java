/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.utils;

import com.micrium.bussiness.ParametroBl;
import com.micrium.bussiness.ParametroID;
import micrium.eventos_bccs.modelo.Cuenta;

/**
 *
 * @author Ricardo Laredo
 */
public class Utils {
	public static Cuenta parceTrama(String trama){
		Cuenta cuenta=new Cuenta();
		String separador=(String)ParametroBl.getParamVal(ParametroID.SEPARADOR_TRAMA);
		if(separador==null||separador.isEmpty()){
			return null;
		}
		String[] ss = trama.trim().split(separador);
		String imsi = ss[0];
		String proceso = ss[3];
		String otros = ss[9];

		cuenta.setIMSI(imsi);
		cuenta.setProceso(proceso);
		cuenta.setOtros(otros);
		//cuenta.setFechaHora();
		return cuenta;
	}
	
}
