/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.model;

import java.io.Serializable;

/**
 *
 * @author SoftWill
 */
public class DriverColaTrama implements Serializable{
	private long id;
	private String tramaBccs;
	private String usuario;
	private String cuenta;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTramaBccs() {
		return tramaBccs;
	}

	public void setTramaBccs(String tramaBccs) {
		this.tramaBccs = tramaBccs;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	
}
