/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.model;

import java.io.Serializable;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverLstCltdsg implements Serializable{
	private long id;
	private int groupNumber;
	private String callerNumber;
	private String addressNature;
	private String functionCode;
	private int minimunNumberLength;
	private int maximunNumberLength;
	private String callSourceName;
	private int chargingSourceCode;
	private int barringGroupNumber;
	private String callerNumberChangeName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getCallerNumber() {
		return callerNumber;
	}

	public void setCallerNumber(String callerNumber) {
		this.callerNumber = callerNumber;
	}

	public String getAddressNature() {
		return addressNature;
	}

	public void setAddressNature(String addressNature) {
		this.addressNature = addressNature;
	}

	public String getFunctionCode() {
		return functionCode;
	}

	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}

	public int getMinimunNumberLength() {
		return minimunNumberLength;
	}

	public void setMinimunNumberLength(int minimunNumberLength) {
		this.minimunNumberLength = minimunNumberLength;
	}

	public int getMaximunNumberLength() {
		return maximunNumberLength;
	}

	public void setMaximunNumberLength(int maximunNumberLength) {
		this.maximunNumberLength = maximunNumberLength;
	}

	public String getCallSourceName() {
		return callSourceName;
	}

	public void setCallSourceName(String callSourceName) {
		this.callSourceName = callSourceName;
	}

	public int getChargingSourceCode() {
		return chargingSourceCode;
	}

	public void setChargingSourceCode(int chargingSourceCode) {
		this.chargingSourceCode = chargingSourceCode;
	}

	public int getBarringGroupNumber() {
		return barringGroupNumber;
	}

	public void setBarringGroupNumber(int barringGroupNumber) {
		this.barringGroupNumber = barringGroupNumber;
	}

	public String getCallerNumberChangeName() {
		return callerNumberChangeName;
	}

	public void setCallerNumberChangeName(String callerNumberChangeName) {
		this.callerNumberChangeName = callerNumberChangeName;
	}

	@Override
	public String toString() {
		return "DriverLstCltdsg{" + "id=" + id + ", groupNumber=" + groupNumber + ", callerNumber=" + callerNumber + ", addressNature=" + addressNature + ", functionCode=" + functionCode + ", minimunNumberLength=" + minimunNumberLength + ", maximunNumberLength=" + maximunNumberLength + ", callSourceName=" + callSourceName + ", chargingSourceCode=" + chargingSourceCode + ", barringGroupNumber=" + barringGroupNumber + ", callerNumberChangeName=" + callerNumberChangeName + '}';
	}
	
	
}
