/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.model;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverEjecucionComando implements Serializable{
	public static final String ESTADO_OK="OK";
	public static final String ESTADO_ERROR="ERROR";
	public static final String ESTADO_PROCESANDO="PROCESANDO";
	private long id;
	private Date fechaRegistro;
	private String cuenta;
	private String tramaBccs;
	
	private String comandoMsc;
	private Date fechaEjecucion;
	private String estado;
	private String usuario;
	private String motivoFallo;
	private String comandoMscFallido;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getCuenta() {
		return cuenta;
	}

	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}

	public String getTramaBccs() {
		return tramaBccs;
	}

	public void setTramaBccs(String tramaBccs) {
		this.tramaBccs = tramaBccs;
	}
	
	public String getComandoMsc() {
		return comandoMsc;
	}

	public void setComandoMsc(String comandoMsc) {
		this.comandoMsc = comandoMsc;
	}

	public Date getFechaEjecucion() {
		return fechaEjecucion;
	}

	public void setFechaEjecucion(Date fechaEjecucion) {
		this.fechaEjecucion = fechaEjecucion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getMotivoFallo() {
		return motivoFallo;
	}

	public void setMotivoFallo(String motivoFallo) {
		this.motivoFallo = motivoFallo;
	}

	public String getComandoMscFallido() {
		return comandoMscFallido;
	}

	public void setComandoMscFallido(String comandoMscFallido) {
		this.comandoMscFallido = comandoMscFallido;
	}
	
	
		
}
