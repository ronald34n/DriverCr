/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.model;

import java.io.Serializable;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverComando implements Serializable{
	private long id;
	private String planComercial;
	private String estadoBccs;
	private String comando;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlanComercial() {
		return planComercial;
	}

	public void setPlanComercial(String planComercial) {
		this.planComercial = planComercial;
	}

	public String getEstadoBccs() {
		return estadoBccs;
	}

	public void setEstadoBccs(String estadoBccs) {
		this.estadoBccs = estadoBccs;
	}

	public String getComando() {
		return comando;
	}

	public void setComando(String comando) {
		this.comando = comando;
	}

	
	
}
