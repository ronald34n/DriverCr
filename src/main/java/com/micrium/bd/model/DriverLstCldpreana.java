/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.model;

import java.io.Serializable;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverLstCldpreana implements Serializable{
	private long id;
	private String callSourceName;
	private String callOriginator;
	private String callPrefix;
	private String callerNumberMinimunLength;
	private String callerNumberAddressNature;
	private String callerRoamingType;
	private String calledNumberChangeName;
	private String newDnSet;
	private String managedObjectGroup;
	private String serverName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCallSourceName() {
		return callSourceName;
	}

	public void setCallSourceName(String callSourceName) {
		this.callSourceName = callSourceName;
	}

	public String getCallOriginator() {
		return callOriginator;
	}

	public void setCallOriginator(String callOriginator) {
		this.callOriginator = callOriginator;
	}

	public String getCallPrefix() {
		return callPrefix;
	}

	public void setCallPrefix(String callPrefix) {
		this.callPrefix = callPrefix;
	}

	public String getCallerNumberMinimunLength() {
		return callerNumberMinimunLength;
	}

	public void setCallerNumberMinimunLength(String callerNumberMinimunLength) {
		this.callerNumberMinimunLength = callerNumberMinimunLength;
	}

	public String getCallerNumberAddressNature() {
		return callerNumberAddressNature;
	}

	public void setCallerNumberAddressNature(String callerNumberAddressNature) {
		this.callerNumberAddressNature = callerNumberAddressNature;
	}

	public String getCallerRoamingType() {
		return callerRoamingType;
	}

	public void setCallerRoamingType(String callerRoamingType) {
		this.callerRoamingType = callerRoamingType;
	}

	public String getCalledNumberChangeName() {
		return calledNumberChangeName;
	}

	public void setCalledNumberChangeName(String calledNumberChangeName) {
		this.calledNumberChangeName = calledNumberChangeName;
	}

	public String getNewDnSet() {
		return newDnSet;
	}

	public void setNewDnSet(String newDnSet) {
		this.newDnSet = newDnSet;
	}

	public String getManagedObjectGroup() {
		return managedObjectGroup;
	}

	public void setManagedObjectGroup(String managedObjectGroup) {
		this.managedObjectGroup = managedObjectGroup;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}



	@Override
	public String toString() {
		return "DriverLstCldpreana{" + "id=" + id + ", callSourceName=" + callSourceName + ", callOriginator=" + callOriginator + ", callPrefix=" + callPrefix + ", callerNumberMinimunLength=" + callerNumberMinimunLength + ", callerNumberAddressNature=" + callerNumberAddressNature + ", callerRoamingType=" + callerRoamingType + ", calledNumberChangeName=" + calledNumberChangeName + ", newDnSet=" + newDnSet + ", managedObjectGroup=" + managedObjectGroup + ", serverName=" + serverName + '}';
	}
	
	
}
