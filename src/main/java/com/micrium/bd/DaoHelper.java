package com.micrium.bd;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * DAOHelper
 *
 * @author Ricardo Laredo
 */


public class DaoHelper<T> extends ConexionBD {

	private static final Logger LOGGER = Logger.getLogger(DaoHelper.class);

	public DaoHelper() {
		super();
	}
	
	

	public interface QueryParameters {

		 void setParameters(PreparedStatement pst) throws SQLException;
	}

	public interface ResultReader<T> {

		 T getResult(ResultSet result) throws SQLException;
	}

	

	public List<T> executeQuery(String query, ResultReader<T> reader) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return executeQuery(query, null, reader);
	}

	public List<T> executeQuery(String query, QueryParameters params, ResultReader<T> reader)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Connection conn;
		try {
			conn = getConnection();
		} catch (Exception ex) {
			LOGGER.info("No se logro crear conexion a la base de datos",ex);
			throw new SQLException(ex);
		}
		PreparedStatement st = conn.prepareStatement(query);
		ResultSet result = null;
		try {
			// let's delegate the control to set the parameters		
			

			if (params != null) {
				params.setParameters(st);
			}
			boolean status = st.execute();
			if (status) {
				List<T> results = new ArrayList<T>();
				result = st.getResultSet();
				while (result.next()) {
					T value = reader.getResult(result);
					if (value != null) {
						results.add(value);
					}
				}
				result.close();
				st.close();
				return results;
			} else {
				st.close();
			}
		} catch (SQLException e) {
			st.close();
			if (result != null) {
				result.close();
			}
			LOGGER.error("[executeQuery] [Error al ejecutar Query]", e);
		} catch (Exception e) {
			st.close();
			if (result != null) {
				result.close();
			}
			LOGGER.error("[executeQuery] [Error interno]", e);
		}
		finally{
			if (!conn.isClosed()) {
				conn.close();
			}
		}
		return null;

	}

	public int executeWrite(String query, QueryParameters params) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Connection conn;
		try {
			conn = getConnection();
		} catch (Exception ex) {
			LOGGER.info("No se logro crear conexion a la base de datos",ex);
			throw new SQLException(ex);
		}
		PreparedStatement st = conn.prepareStatement(query);
		try {
			if (params != null) {
				params.setParameters(st);
			}
			int rowsAffected = st.executeUpdate();
			st.close();
			return rowsAffected;
		} catch (SQLException e) {
			st.close();
			LOGGER.error("[executeWrite] [Error al ejecutar la query] [sqlSentence: " + query + "]", e);
		} catch (Exception e) {
			st.close();
			LOGGER.error("[executeWrite] [Error al ejecutar la query] [sqlSentence: " + query + "]", e);
		}
		finally{
			if (!conn.isClosed()) {
				conn.close();
			}
		}
		return -1;
	}
	public int executeQueryCount(String query, QueryParameters params)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Connection conn;
		try {
			conn = getConnection();
		} catch (Exception ex) {
			LOGGER.info("No se logro crear conexion a la base de datos",ex);
			throw new SQLException(ex);
		}
		PreparedStatement st = conn.prepareStatement(query);
		ResultSet result = null;
		try {		
			if (params != null) {
				params.setParameters(st);
			}
			boolean status = st.execute();
			if (status) {
				int cantRows=-1;
				result = st.getResultSet();
				if(result.next()){
					cantRows=result.getInt(1);
				}
				result.close();
				st.close();
				return cantRows;
			} else {
				st.close();
			}
		} catch (SQLException e) {
			st.close();
			if (result != null) {
				result.close();
			}
			LOGGER.error("[executeQuery] [Error al ejecutar Query]", e);
		} catch (Exception e) {
			st.close();
			if (result != null) {
				result.close();
			}
			LOGGER.error("[executeQuery] [Error interno]", e);
		}
		finally{
			if (!conn.isClosed()) {
				conn.close();
			}
		}
		return -1;

	}

}
