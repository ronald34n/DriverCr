package com.micrium.bd;

import com.micrium.properties.ConexionLocalProperties;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

/**
 *
 * @author Ricardo Laredo
 */
public class ConexionBD implements Serializable {

	private static final long serialVersionUID = 1L;

	//private static final Logger LOGGER = Logger.getLogger(ConexionBD.class);
	private static PoolProperties p = new PoolProperties();
	static {
		setUpConnection();
	}
	private static DataSource datasource = new DataSource(p);

	public Connection getConnection() throws SQLException {
		synchronized (datasource) {
			return datasource.getConnection();
		}
	}

	private static void setUpConnection() {
		synchronized (p) {
			p.setUrl(ConexionLocalProperties.LOCAL_URL);
			p.setDriverClassName(ConexionLocalProperties.LOCAL_DRIVERCLASSNAME);
			p.setUsername(ConexionLocalProperties.LOCAL_USER);
			p.setPassword(ConexionLocalProperties.LOCAL_PASSWORD);
			p.setJmxEnabled(ConexionLocalProperties.LOCAL_JMXENABLED.equalsIgnoreCase("true"));
			p.setTestWhileIdle(ConexionLocalProperties.LOCAL_TESTWHILEIDLE.equalsIgnoreCase("true"));
			p.setTestOnBorrow(ConexionLocalProperties.LOCAL_TESTONBORROW.equalsIgnoreCase("true"));
			p.setTestOnReturn(ConexionLocalProperties.LOCAL_TESTONRETURN.equalsIgnoreCase("true"));
			p.setValidationQuery(ConexionLocalProperties.LOCAL_VALIDATIONQUERY);
			p.setValidationInterval(Long.parseLong(ConexionLocalProperties.LOCAL_VALIDATIONINTERVAL));
			p.setTimeBetweenEvictionRunsMillis(Integer.parseInt(ConexionLocalProperties.LOCAL_TIMEBETWEENEVICTIONRUNSMILLIS));
			p.setMinEvictableIdleTimeMillis(Integer.parseInt(ConexionLocalProperties.LOCAL_MINEVICTABLEIDLETIMEMILLIS));
			p.setMaxActive(Integer.parseInt(ConexionLocalProperties.LOCAL_MAXACTIVE));
			p.setMinIdle(Integer.parseInt(ConexionLocalProperties.LOCAL_MINIDLE));
			p.setMaxIdle(Integer.parseInt(ConexionLocalProperties.LOCAL_MAXIDLE));
			p.setMaxWait(Integer.parseInt(ConexionLocalProperties.LOCAL_MAXWAIT));
			p.setRemoveAbandonedTimeout(Integer.parseInt(ConexionLocalProperties.LOCAL_REMOVEABANDONEDTIMEOUT));
			p.setLogAbandoned(ConexionLocalProperties.LOCAL_LOGABANDONED.equalsIgnoreCase("true"));
			p.setRemoveAbandoned(ConexionLocalProperties.LOCAL_REMOVEABANDONED.equalsIgnoreCase("true"));
			p.setJdbcInterceptors(ConexionLocalProperties.LOCAL_JDBCINTERCEPTORS);
		}
	}

}
