/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.DriverNoticacion;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverNotificacionDao {
	private DaoHelper<DriverNoticacion> daoHelper;

	public DriverNotificacionDao() {
		this.daoHelper=new DaoHelper<>();
	}
	
	public int saveNotificacion(final DriverNoticacion noticacion) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="INSERT INTO "
				+ "DRIVER_NOTIFICACION (ID,CUENTA,FECHA_NOTIFICACION,DESTINATARIOS,ASUNTO,MENSAJE,TIPO) "
				+ " VALUES(DRIVER_SEQ_NOTIFICACION.NEXTVAL,?,SYSDATE,?,?,?,?)";
		DaoHelper.QueryParameters parameters=new DaoHelper.QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, noticacion.getCuenta());
				pst.setString(2, noticacion.getDestinatarios());
				pst.setString(3, noticacion.getAsunto());
				pst.setString(4, noticacion.getMensaje());
				pst.setString(5, noticacion.getTipo());
			}
		};
		return this.daoHelper.executeWrite(query, parameters);
	}
	
	
}
