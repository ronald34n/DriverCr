/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.DriverLogTransaccion;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverLogTransaccionDao extends DaoHelper<DriverLogTransaccion>{
	

	public DriverLogTransaccionDao() {
		super();
	}
	
	public synchronized int saveLogTransaccion(final DriverLogTransaccion log) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="INSERT INTO DRIVER_LOG_TRANSACCION "
				+ "(ID,FECHA_REGISTRO,CUENTA,ACCION,USUARIO,TIPO) "
				+ " VALUES(DRIVER_SEQ_NOTIFICACION.NEXTVAL,SYSDATE,?,?,?,?)";
		DaoHelper.QueryParameters parameters=new DaoHelper.QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, log.getCuenta());
				pst.setString(2, log.getAccion());
				pst.setString(3, log.getUsuario());
				pst.setString(4, log.getTipo());
			}
		};
		return executeWrite(query, parameters);
	}
	
	
}
