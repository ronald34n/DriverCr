/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.DriverComando;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverComandoDao extends DaoHelper<DriverComando> {

	private static final Logger LOGGER = Logger.getLogger(DriverComandoDao.class);
	

	public DriverComandoDao() {
		super();
	}

	public  List<DriverComando> obtenerComando(final String planComercial, final String estadoBccs) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query = "SELECT CC.ID,CC.PLAN_COMERCIAL,CC.ESTADO_BCCS,C.COMANDO FROM DRIVER_CONFIGURACION_COMANDO CC INNER JOIN DRIVER_COMANDO C ON CC.COMANDO_ID=C.ID WHERE CC.PLAN_COMERCIAL=? AND CC.ESTADO_BCCS=? ORDER BY ID ASC";

		DaoHelper.QueryParameters parameters = new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, planComercial);
				pst.setString(2, estadoBccs);
			}
		};
		DaoHelper.ResultReader<DriverComando> reader = new ResultReader() {

			@Override
			public Object getResult(ResultSet result) throws SQLException {
				DriverComando colaTrama = new DriverComando();
				colaTrama.setId(result.getLong("ID"));
				colaTrama.setPlanComercial(result.getString("PLAN_COMERCIAL"));
				colaTrama.setEstadoBccs(result.getString("ESTADO_BCCS"));
				colaTrama.setComando(result.getString("COMANDO"));
				return colaTrama;
			}
		};

		List<DriverComando> list = super.executeQuery(query, parameters, reader);
		if (list == null) {
			return null;
		}
		LOGGER.info("Cantidad de elementos retornados:" + list.size());
		return list;
	}

}
