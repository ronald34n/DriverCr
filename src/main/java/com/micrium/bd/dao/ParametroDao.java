/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.micrium.bd.dao;
import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.MuParametro;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author Ricardo Laredo
 */
public class ParametroDao {
	private com.micrium.bd.DaoHelper<MuParametro> daoHelper;
	public ParametroDao() {
		daoHelper=new DaoHelper<>();
	}
	
//consulta de parametros
    public String obtenerValorParametro(long codigo) throws Exception{
      return null;
    }
	
	public MuParametro obtenerParametro(long codigo) throws Exception{
        return null;
    }
	public List<MuParametro> getAllParameters() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query="SELECT * FROM MU_PARAMETRO";
		DaoHelper.ResultReader<MuParametro> reader=new DaoHelper.ResultReader<MuParametro>() {

			@Override
			public MuParametro getResult(ResultSet result) throws SQLException {
				MuParametro parametro=new MuParametro();
				parametro.setParametroId(result.getLong(1));
				parametro.setNombre(result.getString(2));
				parametro.setTipo(result.getShort(3));
				parametro.setValorcadena(result.getString(4));
				parametro.setValorfecha(result.getDate(5));
				parametro.setValornumerico(result.getBigDecimal(6));
				parametro.setValorbooleano(result.getBoolean(7));
				return parametro;
			}
		};
	 return daoHelper.executeQuery(query, reader);
	}
}
