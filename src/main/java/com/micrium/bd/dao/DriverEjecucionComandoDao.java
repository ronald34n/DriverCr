/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.DriverEjecucionComando;
import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverEjecucionComandoDao extends DaoHelper<DriverEjecucionComando> {

	private static final Logger LOGGER = Logger.getLogger(DriverEjecucionComandoDao.class);

	public DriverEjecucionComandoDao() {
		super();
	}

	public int saveComando(final DriverEjecucionComando colaTrama) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query = "INSERT INTO DRIVER_EJECUCION_COMANDO "
				+ " (ID,FECHA_REGISTRO,CUENTA,TRAMA_BCCS,COMANDO_MSC,ESTADO,USUARIO,FECHA_EJECUCION,MOTIVO_FALLO,COMANDO_MSC_FALLIDO) "
				+ " VALUES(DRIVER_SEQ_EJECUCION_COMANDO.NEXTVAL,SYSDATE,?,?,?,?,?,SYSDATE,?,?)";
		DaoHelper.QueryParameters parameters = new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, colaTrama.getCuenta());
				pst.setString(2, colaTrama.getTramaBccs());
				pst.setString(3, colaTrama.getComandoMsc());
				pst.setString(4, colaTrama.getEstado());
				pst.setString(5, colaTrama.getUsuario());
				pst.setString(6, colaTrama.getMotivoFallo());
				pst.setString(7, colaTrama.getComandoMscFallido());
			}
		};
		return executeWrite(query, parameters);
	}

	public int updateComando(final DriverEjecucionComando colaTrama) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query = "UPDATE DRIVER_EJECUCION_COMANDO "
				+ " SET FECHA_EJECUCION=SYSDATE, MOTIVO_FALLO=?,ESTADO=?,USUARIO=?,COMANDO_MSC_FALLIDO=?"
				+ " WHERE ID=?";
		DaoHelper.QueryParameters parameters = new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, colaTrama.getMotivoFallo());
				pst.setString(2, colaTrama.getEstado());
				pst.setString(3, colaTrama.getUsuario());
				pst.setString(4, colaTrama.getComandoMscFallido());
				pst.setLong(5, colaTrama.getId());
			}
		};
		return executeWrite(query, parameters);
	}

	public DriverEjecucionComando getComandoEjecucion(final String tramaBccs) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query = "SELECT * FROM DRIVER_EJECUCION_COMANDO WHERE TRAMA_BCCS=? AND (ESTADO=? OR ESTADO=?) ORDER BY ID DESC";
		QueryParameters parameters = new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, tramaBccs);
				pst.setString(2, DriverEjecucionComando.ESTADO_ERROR);
				pst.setString(3, DriverEjecucionComando.ESTADO_PROCESANDO);
			}
		};
		ResultReader<DriverEjecucionComando> reader = new ResultReader<DriverEjecucionComando>() {
			@Override
			public DriverEjecucionComando getResult(ResultSet result) throws SQLException {
				DriverEjecucionComando comando = new DriverEjecucionComando();
				comando.setId(result.getLong("ID"));
				comando.setCuenta(result.getString("CUENTA"));
				comando.setTramaBccs(result.getString("TRAMA_BCCS"));
				comando.setEstado(result.getString("ESTADO"));
				comando.setUsuario(result.getString("USUARIO"));
				comando.setFechaRegistro(result.getDate("FECHA_REGISTRO"));
				comando.setComandoMscFallido(result.getString("COMANDO_MSC_FALLIDO"));
				return comando;
			}
		};

		List<DriverEjecucionComando> list = executeQuery(query, parameters, reader);
		if (list == null) {
			LOGGER.error("Existio una excepcion al obtener lista de DRIVER_EJECUCION_COMANDO, Se esperaba que exista el elemento o que no exista");
			return null;
		}
		
		if (list.size() > 1) {
			LOGGER.warn("No deberia existir mas de un elemento con la misma trama en la tabla DRIVER_EJECUCION_COMANDO: "+list.size());
		}
		
		if (list.size() > 0) {
			return list.get(0);
		}
		
		return null;
	}
}
