/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.DriverColaTrama;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverColaTramaDao extends DaoHelper<DriverColaTrama>{

	public DriverColaTramaDao() {
		super();
	}
	
	public int encolar(final DriverColaTrama colaTrama) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="INSERT INTO DRIVER_COLA_TRAMA "
				+ " (ID,TRAMA_BCCS,USUARIO,CUENTA) VALUES(DRIVER_SEQ_COLA_TRAMA.NEXTVAL,?,?,?)";
		DaoHelper.QueryParameters parameters=new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, colaTrama.getTramaBccs());
				pst.setString(2, colaTrama.getUsuario());
				pst.setString(3, colaTrama.getCuenta());
			}
		};
		return executeWrite(query, parameters);
	}
	
	public  synchronized DriverColaTrama obtenerTrama() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="SELECT * FROM DRIVER_COLA_TRAMA WHERE ID=(SELECT MIN(ID) FROM DRIVER_COLA_TRAMA )";
		
		DaoHelper.ResultReader<DriverColaTrama> reader=new ResultReader() {

			@Override
			public Object getResult(ResultSet result) throws SQLException {
				DriverColaTrama colaTrama=new DriverColaTrama();
				colaTrama.setId(result.getLong("ID"));
				colaTrama.setTramaBccs(result.getString("TRAMA_BCCS"));
				colaTrama.setCuenta(result.getString("CUENTA"));
				colaTrama.setUsuario(result.getString("USUARIO"));
				return colaTrama;
			}
		};
		
		List<DriverColaTrama> list= super.executeQuery(query, reader);
		if(list==null){
			return null;
		}
		if(list.size()==1){
			DriverColaTrama colaTrama=list.get(0);
			borrarElemento(colaTrama);
			return colaTrama;
		}
		return null;
	}
	
	private int borrarElemento(final DriverColaTrama colaTrama) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="DELETE FROM DRIVER_COLA_TRAMA WHERE ID=?";
		QueryParameters  parameters=new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setLong(1, colaTrama.getId());
			}
		};
		
		return executeWrite(query, parameters);
	}
	
	public int contarCantidadEnCola() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="SELECT COUNT(*) AS CANTIDAD FROM DRIVER_COLA_TRAMA";
		return executeQueryCount(query, null);
	}
	
	
}
