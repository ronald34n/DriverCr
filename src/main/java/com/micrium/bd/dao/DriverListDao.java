/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import com.micrium.bd.model.DriverLstCldpreana;
import com.micrium.bd.model.DriverLstCltdsg;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverListDao {
	private DaoHelper daoHelper;

	public DriverListDao() {
		this.daoHelper=new DaoHelper();
	}
	
	 public int saveLstCltdsg(final DriverLstCltdsg cldpreana) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="INSERT INTO DRIVER_LST_CLRDSG(ID,GROUP_NUMBER,CALLER_NUMBER,ADDRESS_NATURE,FUNCTION_CODE,MINIMUN_NUMBER_LENGTH,MAXIMUN_NUMBER_LENGTH,CALL_SOURCE_NAME,CHARGING_SOURCE_CODE,BARRING_GROUP_NUMBER,CALLER_NUMBER_CHANGE_NAME,FECHA_REGISTRO)VALUES(DRIVER_SEQ_LST_CLRDSG.NEXTVAL,?,?,?,?,?,?,?,?,?,?,SYSDATE)";
		DaoHelper.QueryParameters parameters=new DaoHelper.QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setInt(1, cldpreana.getGroupNumber());
				pst.setString(2, cldpreana.getCallerNumber());
				pst.setString(3, cldpreana.getAddressNature());
				pst.setString(4, cldpreana.getFunctionCode());
				pst.setInt(5, cldpreana.getMinimunNumberLength());
				pst.setInt(6, cldpreana.getMaximunNumberLength());
				pst.setString(7, cldpreana.getCallSourceName());
				pst.setInt(8, cldpreana.getChargingSourceCode());
				pst.setInt(9, cldpreana.getBarringGroupNumber());
				pst.setString(10, cldpreana.getCallerNumberChangeName());
				
			}
		};
		return this.daoHelper.executeWrite(query, parameters);
	}
	
	public int saveLstCldpreana(final DriverLstCldpreana cldpreana) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		String query="INSERT INTO DRIVER_LST_CLDPREANA( ID,CALL_SOURCE_NAME,CALL_ORIGINATOR,CALL_PREFIX,CALLER_NUMBER_MINIMUN_LENGTH,CALLER_NUMBER_ADDRESS_NATURE,CALLER_ROAMING_TYPE,CALLED_NUMBER_CHANGE_NAME,NEW_DN_SET,MANAGED_OBJECT_GROUP,SERVER_NAME,FECHA_REGISTRO) VALUES(DRIVER_SEQ_LST_CLDPREANA.NEXTVAL,?,?,?,?,?,?,?,?,?,?,SYSDATE)";
		DaoHelper.QueryParameters parameters=new DaoHelper.QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setString(1, cldpreana.getCallSourceName());
				pst.setString(2, cldpreana.getCallOriginator());
				pst.setString(3, cldpreana.getCallPrefix());
				pst.setString(4, cldpreana.getCallerNumberMinimunLength());
				pst.setString(5, cldpreana.getCallerNumberAddressNature());
				pst.setString(6, cldpreana.getCallerRoamingType());
				pst.setString(7, cldpreana.getCalledNumberChangeName());
				pst.setString(8, cldpreana.getNewDnSet());
				pst.setString(9, cldpreana.getManagedObjectGroup());
				pst.setString(10, cldpreana.getServerName());
				
			}
		};
		return this.daoHelper.executeWrite(query, parameters);
	}
	
}
