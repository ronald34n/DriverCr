/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.bd.dao;

import com.micrium.bd.DaoHelper;
import org.apache.log4j.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Ricardo Laredo
 */
public class DriverRangoTelefoniaDao extends DaoHelper<String> {
	private static final Logger LOGGER = Logger.getLogger(DriverRangoTelefoniaDao.class);

	public DriverRangoTelefoniaDao() {
		super();
	}

	public  String obtenerPlanComercial(final int number) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		String query = "SELECT PLAN_COMERCIAL FROM DRIVER_RANGO_TELEFONIA WHERE ESTADO='AC' AND ? BETWEEN RANGO_INICIO AND RANGO_FIN";

		QueryParameters parameters = new QueryParameters() {

			@Override
			public void setParameters(PreparedStatement pst) throws SQLException {
				pst.setInt(1, number);
			}
		};
		ResultReader<String> reader = new ResultReader() {
			@Override
			public Object getResult(ResultSet result) throws SQLException {
				return result.getString("PLAN_COMERCIAL");
			}
		};

		List<String> list = super.executeQuery(query, parameters, reader);
		if (list == null||list.isEmpty()) {
			return "No corresponde a ningun plan";
		}
		LOGGER.info("Se encontro el plan comercial al que pertence el numero:" + number);
		return list.get(0);
	}

}
