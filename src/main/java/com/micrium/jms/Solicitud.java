package com.micrium.jms;

/**
 *
 * @author Ricardo Laredo
 */
public class Solicitud {

	private long id = 0;
	private String nroCuentaA = "";
	private String nroCuentaB = "";
	private String imsi = "";
	private String proceso = "";
	private String fechaHora = null;
	private String billetera = "";
	private String monto = "";
	private String vigencia = "";
	private String comentario = "";
	private String otros = "";
	private int tipo = 0;

	public Solicitud() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNroCuentaA() {
		return nroCuentaA;
	}

	public void setNroCuentaA(String nroCuentaA) {
		this.nroCuentaA = nroCuentaA;
	}

	public String getNroCuentaB() {
		return nroCuentaB;
	}

	public void setNroCuentaB(String nroCuentaB) {
		this.nroCuentaB = nroCuentaB;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getBilletera() {
		return billetera;
	}

	public void setBilletera(String billetera) {
		this.billetera = billetera;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getOtros() {
		return otros;
	}

	public void setOtros(String otros) {
		this.otros = otros;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

}
