package com.micrium.jms;

import com.micrium.bd.dao.DriverColaTramaDao;
import com.micrium.bd.model.DriverColaTrama;
import com.micrium.bussiness.LogTransaccion;
import com.micrium.bussiness.ParametroBl;
import com.micrium.bussiness.ParametroID;
import com.micrium.bussiness.PlanificadorEjecucion;
import com.micrium.properties.ConfiguracionProperties;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Pattern;
import micrium.eventos_bccs.modelo.Cuenta;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class HiloProcesador extends Thread {

	private static final Logger LOGGER = Logger.getLogger(HiloProcesador.class);

	private final String trama;
	private Cuenta cuenta;
	private DriverColaTramaDao colaTramaDao;
	
	public HiloProcesador(String trama) throws Exception {
		this.trama = trama;
		this.colaTramaDao = new DriverColaTramaDao();
		this.cuenta=tramaToCuenta();
		if(cuenta==null){
			throw new Exception("No se logro parcear la trama a Cuenta");
		}
	}

	public HiloProcesador(Cuenta cuenta) {
		this.cuenta = cuenta;
		this.trama = this.cuenta.toTrama();
		this.colaTramaDao = new DriverColaTramaDao();
	}

	@Override
	public void run() {
		LOGGER.info("[trama: " + trama + "] [Se procesara una nueva trama]");
		if (cuenta != null) {
			LOGGER.info("La cola recibio un Objeto");
			try {
				procesarTrama();
			} catch (Exception e) {
				LOGGER.error("[trama: " + trama + "] [Fallo al procesar la trama]", e);
			}

		} else {
			if (validarTrama()) {
				LOGGER.info("[trama: " + trama + "] [Validacion correcta de la trama con la expresion regular: " + ConfiguracionProperties.EXPRESION_VALIDAR_TRAMA + "]");
				if (validarCantidadParametros()) {
					LOGGER.info("[trama: " + trama + "] [Validacion correcta de la cantidad de parametros en la trama, se procedera a procesar la trama]");
					try {
						procesarTrama();
					} catch (Exception e) {
						LOGGER.error("[trama: " + trama + "] [Fallo al procesar la trama]", e);
					}
				} else {
					LOGGER.warn("[trama: " + trama + "] [Fallo la validacion de la trama, cantidad de parametros esperado: " + ConfiguracionProperties.CANTIDAD_PARAMETROS + "]");
				}
			} else {
				LOGGER.warn("[trama: " + trama + "] [Fallo la validacion de la trama, expresion regular: " + ConfiguracionProperties.EXPRESION_VALIDAR_TRAMA + "]");
			}
		}
		
	}

	private void procesarTrama() throws ParseException{
		String procesoDriver=(String)ParametroBl.getParamVal(ParametroID.NOMBRE_PROCESO_DRIVER);
		if(cuenta==null){
			LOGGER.error("Se recibio una trama en modo texto y al procesar hubo un error que no se puede recupera");
			return;
		}
		if(!cuenta.getProceso().equals(procesoDriver)){
			LOGGER.info("Se detecto una trama que no corresponde a Driver");
			return;
		}
		
		DriverColaTrama colaTrama = new DriverColaTrama();
		colaTrama.setCuenta(cuenta.getNroCuentaA());
		colaTrama.setTramaBccs(cuenta.toTrama());
		colaTrama.setUsuario("DRIVER");
		try {
			if (colaTramaDao.encolar(colaTrama) > 0) {
				PlanificadorEjecucion.notificar();
				LOGGER.info("Se encolo la trama correctamente en la DB para ser procesado");
				LogTransaccion.info("Se encolo la trama correctamente en la DB para ser procesado",cuenta.getNroCuentaA());
			}
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
			LOGGER.error("No se logro encolar la trama en la tabla DRIVER_COLA_TRAMA", ex);
			LogTransaccion.error("No se logro encolar la trama en la tabla DRIVER_COLA_TRAMA",cuenta.getNroCuentaA());
		}

	}

	private boolean validarTrama() {
		return trama != null && Pattern.matches(ConfiguracionProperties.EXPRESION_VALIDAR_TRAMA, trama);
	}

	private boolean validarCantidadParametros() {
		if (trama != null) {
			String separador=(String)ParametroBl.getParamVal(ParametroID.SEPARADOR_TRAMA);
			if(separador==null||separador.isEmpty()){
				LOGGER.error("El parametro SEPARADOR_TRAMA es null");
				return false;
			}
			String[] ss = trama.trim().split(separador);
			return ss.length == ConfiguracionProperties.CANTIDAD_PARAMETROS;
		}
		return false;
	}
	
	private Cuenta tramaToCuenta() {
		Cuenta mCuenta=new Cuenta();

		String separador=(String)ParametroBl.getParamVal(ParametroID.SEPARADOR_TRAMA);
		if(separador==null||separador.isEmpty()){
			LOGGER.error("El parametro SEPARADOR_TRAMA es null");
			return null;
		}

		String[] ss = trama.trim().split(separador);
		try {
			String nroCuentaA = ss[0];
			String proceso = ss[3];
			String fechaHora = ss[4];
			String otros = ss[9];
			mCuenta.setIMSI(nroCuentaA);
			mCuenta.setProceso(proceso);
			SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(format.parse(fechaHora));
			mCuenta.setFechaHora(calendar);
			mCuenta.setOtros(otros);
		} catch (ParseException ex) {
			LOGGER.error("Error al parcear la fecha: "+ss[4], ex);
			LogTransaccion.error("Error al parcear la fecha: "+ss[4], "SN");
			return null;
		}catch (Exception e){
			LOGGER.error("Error al convertir a Cuenta: "+trama,e);
			return null;
		}
		return mCuenta;
	}

}
