package com.micrium.jms;

import com.micrium.bussiness.LogTransaccion;
import com.micrium.bussiness.ParametroBl;
import com.micrium.bussiness.ParametroID;
import java.util.Properties;
import javax.jms.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import micrium.eventos_bccs.modelo.Cuenta;
import org.apache.log4j.Logger;

public class TopicReceive implements MessageListener {

	private static final Logger LOGGER = Logger.getLogger(TopicReceive.class);

	private TopicConnectionFactory topicConnectionFactory;
	private TopicConnection topicConnection;
	private Session session;
	private MessageConsumer messageConsumer;
	private Topic topic;

	public TopicReceive() throws NamingException, JMSException {
		InitialContext ic = getInitialContext();
		if(ic==null){
			LOGGER.error("No se logro crear el contexto porque los parametros de iniciacion fueron nullos al obtener de la base de datos");
			throw new JMSException("No se logro crear el contexto porque los parametros de iniciacion fueron nullos al obtener de la base de datos");
		}
		Object object=ParametroBl.getParamVal(ParametroID.NAME_QUEUE_TOPIC);
		if(object==null){
			LOGGER.error("El parametro NAME_QUEUE_TOPIC es null");
			throw new JMSException("No se logro crear el contexto porque los parametros de iniciacion fueron nullos al obtener de la base de datos");
		}
		init(ic, (String)object);
	}

	@Override
	public void onMessage(Message msg) {
		try {
			HiloProcesador hp;
			String msgText;
			if (msg instanceof TextMessage) {
				LOGGER.info("Se recibe un TextMessage");
				msgText = ((TextMessage) msg).getText();
				try {
					hp = new HiloProcesador(msgText);
					hp.start();
				} catch (Exception ex) {
					LOGGER.error("Error al iniciar el hilo procesador", ex);
				}

			} else if (msg instanceof ObjectMessage) {
				try {
					msgText = ((Cuenta) ((ObjectMessage) msg).getObject()).toTrama();
					Cuenta cuenta = (Cuenta) ((ObjectMessage) msg).getObject();
					hp = new HiloProcesador(cuenta);
					hp.start();
					LOGGER.info("[Llega un objeto cuenta y se transforma a trama: " + msgText + "]");
				} catch (Exception e) {
					LOGGER.error("[Fallo al obtener el objeto cuenta de la trama]", e);
					msgText = "";
				}
			} else {
				LOGGER.info("No es un objeto ni TextMessage");
				msgText = msg.toString();
				try {
					hp = new HiloProcesador(msgText);
					hp.start();
				} catch (Exception ex) {
					LOGGER.error("Error al iniciar el hilo procesador", ex);
				}

			}
		} catch (JMSException jmse) {
			LOGGER.error("[Fallo al obtener el mensaje desde la cola]", jmse);
		}
	}

	private void init(final Context ctx, final String queueName) throws NamingException, JMSException {
		final String jmsRemoteConnectionFactory = (String) ParametroBl.getParamVal(ParametroID.JMS_REMOTE_CONNECTION_FACTORY);
		final String jbossAplicationUser = (String) ParametroBl.getParamVal(ParametroID.JBOSS_APPLICATION_USER);
		final String jbossAplicationPassword = (String) ParametroBl.getParamVal(ParametroID.JBOSS_APPLICATION_PASSWORD);

		topicConnectionFactory = (TopicConnectionFactory) ctx.lookup(jmsRemoteConnectionFactory);
		topicConnection = topicConnectionFactory.createTopicConnection(jbossAplicationUser, jbossAplicationPassword);
		session = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
		topic = (Topic) ctx.lookup(queueName);
		messageConsumer = session.createConsumer(topic);
		messageConsumer.setMessageListener(this);
		topicConnection.start();
		topicConnection.setExceptionListener(new ExceptionListener() {

			@Override
			public void onException(JMSException jmse) {
				if (jmse.getErrorCode().equals("DISCONNECT")) {
					LOGGER.error("Conexion con el servidor de cola termino, posiblemente el servidor de cola cayo", jmse);
					LogTransaccion.error("Conexion con el servidor de cola termino, posiblemente el servidor de cola cayo", "SN");
					
					boolean connected = false;
				try {
						topicConnection.close();
						ctx.close();
						session.close();
						messageConsumer.close();
					} catch (JMSException | NamingException e) {
						LOGGER.error("Error al cerrar la instancia hacia el servidor de colas\n"
								+ "Se aconseja reiniciar el esta aplicacion (DriverCr) una vez que \n"
								+ "reinicio que se asegure que el servidor de cola se este ejecutanto correctamente",e);
						return;
					}
				while (!connected) {
					try {
							InitialContext ic = getInitialContext();
							if(ic==null){
								throw new JMSException("No se logro crear el contexto porque los parametros de iniciacion fueron nullos al obtener de la base de datos");
							}
							topicConnectionFactory = (TopicConnectionFactory) ic.lookup(jmsRemoteConnectionFactory);
							topicConnection = topicConnectionFactory.createTopicConnection(jbossAplicationUser, jbossAplicationPassword);
							session = topicConnection.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
							topic = (Topic) ic.lookup(queueName);
							messageConsumer = session.createConsumer(topic);
							messageConsumer.setMessageListener(TopicReceive.this);
							topicConnection.setExceptionListener(this);
							topicConnection.start();
							LOGGER.info("********************** RECONEXION EXITOSO **********************");
							connected = true;
						} catch (JMSException | NamingException ex) {
							LOGGER.error("#################### Error al reconectar ####################");
							//LOGGER.error(ex);
						}
				}
					
				}
			}
		});
	}

	private static InitialContext getInitialContext() throws NamingException {
		String jbossAplicationUser = (String) ParametroBl.getParamVal(ParametroID.JBOSS_APPLICATION_USER);
		String jbossAplicationPassword = (String) ParametroBl.getParamVal(ParametroID.JBOSS_APPLICATION_PASSWORD);
		String remoteUrl = (String) ParametroBl.getParamVal(ParametroID.REMOTE_URL);
		String initialContextFactory = (String) ParametroBl.getParamVal(ParametroID.INITIAL_CONTEXT_FACTORY);
		LOGGER.info("Conectando al servidor de colas: " + remoteUrl);
		if(jbossAplicationPassword==null||jbossAplicationPassword.isEmpty()){
			LOGGER.error("El parametro JBOSS_APPLICATION_PASSWORD es null");
			return null;
		}
		if(jbossAplicationUser==null||jbossAplicationUser.isEmpty()){
			LOGGER.error("El parametro JBOSS_APPLICATION_USER");
			return null;
		}
		if(remoteUrl==null||remoteUrl.isEmpty()){
			LOGGER.error("El parametro REMOTE_URL es null");
			return null;
		}
		if(initialContextFactory==null||initialContextFactory.isEmpty()){
			LOGGER.error("El parametro INITIAL_CONTEXT_FACTORY es null");
			return null;
		}

		Properties p = new Properties();
		p.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
		p.put(Context.PROVIDER_URL, remoteUrl);
		p.put(Context.SECURITY_PRINCIPAL, jbossAplicationUser);
		p.put(Context.SECURITY_CREDENTIALS, jbossAplicationPassword);
		return new InitialContext(p);
	}

}
