/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.telnet;

import java.io.IOException;

/**
 *
 * @author Ricardo Laredo
 */
public interface ClienteMsc {
	
	 String login() throws Exception,IOException;
	 String sendComandInit() throws Exception,IOException;
	 String sendComando(String comando) throws IOException;
	 void sendComandoToList(String comando) throws IOException;
	 String readLine()throws IOException;
	 void close();
}
