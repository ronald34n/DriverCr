/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.micrium.telnet;

import com.micrium.bussiness.ParametroBl;
import com.micrium.bussiness.ParametroID;
import java.io.IOException;
import java.math.BigDecimal;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import org.apache.log4j.Logger;

/**
 *
 * @author Ricardo Laredo
 */
public class ClienteSSL implements ClienteMsc {
	public final static String CRLF = "\n\r";
	private static final Logger LOGGER = Logger.getLogger(ClienteSSL.class);
	public static final String PRONT_LOGIN = "Success";
	public static final String PRONT_COMANDO_SUCCESS = "succeeded";
	public static final String PRONT_INIT_COMAND_SUCCESS = "RETCODE = 0  Success";
	public static final String COMAND_INIT = "REG NE:NAME=\"MSC-PRG\";";
	private SSLSocket socket;
	private String ip = "";
	private int puerto = -1;
	private String user = "";
	private String password = "";
	private PrintWriter out;
	private BufferedReader bf;
	private boolean mscHabilitado;

	TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
		@Override
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		@Override
		public void checkClientTrusted(X509Certificate[] certs, String authType) {
		}

		@Override
		public void checkServerTrusted(X509Certificate[] certs, String authType) {
		}
	}
	};

	public ClienteSSL() throws  Exception {
		this.mscHabilitado = (boolean) ParametroBl.getParamVal(ParametroID.MSC_HABILITADO);

		Object obj= ParametroBl.getParamVal(ParametroID.MSC_IP);
		if(obj==null){
			LOGGER.error("El parametro IP del servidor MSC es null");
			return;
		}
		this.ip=(String)obj;

		obj=ParametroBl.getParamVal(ParametroID.MSC_PUERTO);
		if(obj==null){
			LOGGER.error("El parametro IP del servidor MSC es null");
			return;
		}
		this.puerto = ((BigDecimal)obj).intValue();


		if (this.ip == null || this.ip.isEmpty()) {
			LOGGER.error("IP del servidor MSC es null o vacio");
			throw new Exception("IP del servidor MSC es null o vacio");
		}
		if (this.puerto == -1) {
			LOGGER.error("El puerto del servidor MSC tiene valor por defecto -1");
			throw new Exception("El puerto del servidor MSC tiene valor por defecto -1");
		}
		LOGGER.info("Conectando al servidor MSC [" + ip + ":" + puerto + "]");

		if (!mscHabilitado) {
			LOGGER.info("Servidor MSC no esta habilitado");
		} else {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, null);
			SSLSocketFactory ssf = sc.getSocketFactory();
			this.socket = (SSLSocket) ssf.createSocket(this.ip, puerto);
			this.socket.setEnabledProtocols(new String[]{"SSLv3"});

			this.out = new PrintWriter(this.socket.getOutputStream());
			this.bf = new BufferedReader(new InputStreamReader(socket.getInputStream(),Charset.forName("UTF-8")));
			LOGGER.info("Conexion exitosa al servidor MSC");
		}
	}

	@Override
	public String login() throws Exception,IOException  {
		if (!mscHabilitado) {
			LOGGER.info("El servidor MSC no esta habilitado");
			return PRONT_LOGIN;
		}
		this.user = (String) ParametroBl.getParamVal(ParametroID.MSC_USUARIO);
		this.password = (String) ParametroBl.getParamVal(ParametroID.MSC_PASSWORD);

		if (this.user==null||this.user.isEmpty()) {
			LOGGER.error("El usuario del servidor MSC esta vacio");
			throw new Exception("El usuario del servidor MSC esta vacio");
		}
		if (this.password==null||this.password.isEmpty()) {
			LOGGER.error("El password del servidor MSC esta vacio");
			throw new Exception("El password del servidor MSC esta vacio");
		}
		String loginIn = "LGI: OP=\"%USER%\", PWD=\"%PASSWORD%\";";
		loginIn = loginIn.replace("%USER%", user);
		loginIn = loginIn.replace("%PASSWORD%", password);
		out.println(loginIn + CRLF);
		out.flush();
		LOGGER.info("Comando login enviado correctamente: " + loginIn);

		StringBuilder sb = new StringBuilder(CRLF);
		String pron;
		while ((pron = bf.readLine()) != null) {
			LOGGER.info(pron);
			sb.append(pron);
			sb.append(CRLF);
			if (pron.contains("END")) {
				return sb.toString();
			}
		}

		return "";
	}

	@Override
	public String sendComandInit() throws IOException {
		if (!this.mscHabilitado) {
			return PRONT_INIT_COMAND_SUCCESS;
		}
		out.println(COMAND_INIT+CRLF);
		out.flush();
		StringBuilder sb = new StringBuilder();
		String pron;
		while ((pron = bf.readLine()) != null) {
			sb.append(pron);
			sb.append(CRLF);
			if (pron.contains("END")) {
				return sb.toString();
			}
		}
		return "";
	}

	@Override
	public String sendComando(String comando) throws IOException {
		if (!this.mscHabilitado) {
			return ClienteSSL.PRONT_COMANDO_SUCCESS;
		}
		out.println(comando+CRLF);
		out.flush();
		LOGGER.info("Comando: " + comando + " ejecutado correctamente");
		StringBuilder sb = new StringBuilder();
		String pron;
		while ((pron = bf.readLine()) != null) {
			sb.append(pron);
			sb.append(CRLF);
			if (pron.contains("END")) {
				return sb.toString();
			}
		}
		return "";
	}

	@Override
	public void sendComandoToList(String comando) throws IOException {
		if (this.mscHabilitado) {
			out.println(comando+CRLF);
			out.flush();
			LOGGER.info("Comando: " + comando + " ejecutado correctamente");
		}
	}

	@Override
	public String readLine() throws IOException {
		if (!this.mscHabilitado) {
			return PRONT_COMANDO_SUCCESS;
		}
		return bf.readLine();
	}

	@Override
	public void close() {
		if (socket != null && socket.isConnected()) {
			try {
				socket.close();
			} catch (IOException ex) {
				LOGGER.error("No se logro cerrar la conexion con el servidor MSC", ex);
				return;
			}
		}
		if (out != null) {
			out.close();
		}

		if (bf != null) {
			try {
				bf.close();
			} catch (IOException ex) {
				LOGGER.error("No se logro cerrar el buffere de escritura", ex);
			}
		}
	}

}
